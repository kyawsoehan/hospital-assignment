CREATE TABLE IF NOT EXISTS patients(
    id VARCHAR(100) PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    age NUMERIC(3) NOT NULL,
    gender CHAR(1) NOT NULL
);