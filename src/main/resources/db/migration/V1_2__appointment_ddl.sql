CREATE TABLE IF NOT EXISTS appointments(
    id                              VARCHAR(50) PRIMARY KEY NOT NULL,
    patient_id                      VARCHAR(100) NOT NULL REFERENCES patients(id),
    doctor_id                       VARCHAR(100) NOT NULL REFERENCES doctors(id),
    appointment_time                TIMESTAMP NOT NULL,
    appointment_end_time            TIMESTAMP NOT NULL,
    created_at                      TIMESTAMP NOT NULL,
    status                          VARCHAR(30) NOT NULL,
    created_by_doctor_id            VARCHAR(100) REFERENCES doctors(id),
    created_by_patient_id           VARCHAR(100) REFERENCES patients(id)
);

CREATE INDEX appointment_time_patient_id ON appointments(appointment_time, patient_id);
CREATE INDEX appointment_time_doctor_id ON appointments(appointment_time, doctor_id);