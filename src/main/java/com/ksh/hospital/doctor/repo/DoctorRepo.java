package com.ksh.hospital.doctor.repo;

import com.ksh.hospital.doctor.entity.Doctor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepo extends CrudRepository<Doctor, String> {
}
