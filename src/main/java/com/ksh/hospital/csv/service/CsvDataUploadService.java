package com.ksh.hospital.csv.service;

import com.ksh.hospital.CommonConstants;
import com.ksh.hospital.appointment.entity.Appointment;
import com.ksh.hospital.appointment.entity.AppointmentStatus;
import com.ksh.hospital.appointment.repo.AppointmentRepo;
import com.ksh.hospital.doctor.entity.Doctor;
import com.ksh.hospital.doctor.repo.DoctorRepo;
import com.ksh.hospital.patient.entity.Gender;
import com.ksh.hospital.patient.entity.Patient;
import com.ksh.hospital.patient.repo.PatientRepo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class CsvDataUploadService {

    private static final String [] CSV_HEADERS = {
            "doctor_id",
            "doctor_name",
            "patient_id",
            "patient_name",
            "patient_age",
            "patient_gender",
            "appointment_id",
            "appointment_datetime"
    };

    private final DoctorRepo doctorRepo;

    private final PatientRepo patientRepo;

    private final AppointmentRepo appointmentRepo;

    @Transactional
    public Integer extractAndSave(InputStream inputStream) throws IOException {

        Iterable<CSVRecord> csvRecords = CSVFormat.DEFAULT
                .withHeader(CSV_HEADERS)
                .withTrim()
                .withFirstRecordAsHeader()
                .parse(new InputStreamReader(inputStream));


        Set<Doctor> doctors = new HashSet<>();
        Set<Patient> patients = new HashSet<>();
        List<Appointment> appointments = new ArrayList<>();

        csvRecords.forEach(csvRecord -> {

            Doctor doctor = this.getDoctor(csvRecord);
            Patient patient = this.getPatient(csvRecord);
            Appointment appointment = this.getAppointment(doctor, patient, csvRecord);

            doctors.add(doctor);
            patients.add(patient);
            appointments.add(appointment);

        });

        if(!CollectionUtils.isEmpty(doctors)) doctorRepo.saveAll(doctors);
        if(!CollectionUtils.isEmpty(patients)) patientRepo.saveAll(patients);
        if(!CollectionUtils.isEmpty(appointments)) appointmentRepo.saveAll(appointments);

        return appointments.size();
    }

    private Doctor getDoctor(CSVRecord csvRecord){
        return Doctor.builder()
                .id(csvRecord.get("doctor_id"))
                .name(csvRecord.get("doctor_name"))
                .build();
    }

    private Patient getPatient(CSVRecord csvRecord){
        return Patient.builder()
                .id(csvRecord.get("patient_id"))
                .name(csvRecord.get("patient_name"))
                .age(Integer.parseInt(csvRecord.get("patient_age")))
                .gender(csvRecord.get("patient_gender").equals("M") ? Gender.M : Gender.F)
                .build();
    }

    private Appointment getAppointment(Doctor doctor, Patient patient, CSVRecord csvRecord){
        Instant appointmentTime = LocalDateTime.parse(
                csvRecord.get("appointment_datetime"),
                DateTimeFormatter.ofPattern("ddMMyyyy HH:mm:ss")
            )
            .atZone(ZoneId.of("Asia/Singapore"))
            .toInstant();
        return Appointment.builder()
                .id(csvRecord.get("appointment_id"))
                .appointmentTime(appointmentTime)
                .appointmentEndTime(appointmentTime.plusSeconds(CommonConstants.APPOINTMENT_DURATION_SECONDS))
                .doctor(doctor)
                .patient(patient)
                .status(AppointmentStatus.ACTIVE)
                .createdAt(Instant.now())
                .build();
    }

}
