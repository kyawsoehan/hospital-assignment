package com.ksh.hospital.csv.controller;

import com.ksh.hospital.csv.service.CsvDataUploadService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequestMapping("csv")
@RequiredArgsConstructor
public class CsvController {

    private final CsvDataUploadService service;

    @PostMapping("upload")
    @ResponseBody
    public String uploadData(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        return service.extractAndSave(multipartFile.getInputStream()) + " of Appointments have been saved!";
    }

}
