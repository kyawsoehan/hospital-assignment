package com.ksh.hospital.appointment.service;

import com.ksh.hospital.CommonConstants;
import com.ksh.hospital.appointment.entity.Appointment;
import com.ksh.hospital.appointment.entity.AppointmentStatus;
import com.ksh.hospital.appointment.exception.AppointmentOccupiedException;
import com.ksh.hospital.appointment.exception.InvalidAppointmentTimeException;
import com.ksh.hospital.appointment.model.AppointmentCreateRequest;
import com.ksh.hospital.appointment.repo.AppointmentRepo;
import com.ksh.hospital.doctor.entity.Doctor;
import com.ksh.hospital.patient.entity.Patient;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Service
@Log4j2
@RequiredArgsConstructor
public class AppointmentService {

    private final AppointmentRepo repo;

    private static final String CURRENT_TIME_ZONE = "Asia/Singapore";

    private static final int TOTAL_SECOND_IN_DAY = 24 * 60 * 60;

    public List<Appointment> getAppointmentsByDoctorByDate(String doctorId, Instant startTime){
        Instant endTime = plusOneDay(startTime);
        return repo.findByDoctorIdWithTime(doctorId, startTime, endTime);
    }

    public List<Appointment> getAppointmentsByPatientByDate(String patientId, Instant startTime){
        Instant endTime = plusOneDay(startTime);
        return repo.findByPatientIdWithTime(patientId, startTime, endTime);
    }

    @Transactional
    public void cancelAppointment(String appointmentId){
        Integer updatedCount = repo.cancelAppointment(appointmentId);
        log.info("Cancelled Appointment ID {} -> Updated Count {}", appointmentId, updatedCount);
    }

    @Transactional
    public Appointment createAppointment(AppointmentCreateRequest req){

        Instant endTime = req.getTime().plusSeconds(CommonConstants.APPOINTMENT_DURATION_SECONDS);

        if(!isTimeOkay(req.getTime(), endTime)){
            throw new InvalidAppointmentTimeException("Time is invalid in " + CURRENT_TIME_ZONE + " timezone");
        }

        List<Appointment> occupiedAppointments = repo.findBookedAppointments(
                req.getTime(),
                endTime,
                req.getDoctorId(),
                req.getPatientId()
        );

        if(!CollectionUtils.isEmpty(occupiedAppointments)){
            throw new AppointmentOccupiedException("Appointment Already Occupied", occupiedAppointments);
        }

        return repo.save(this.transformAppmtCreateReq(req, endTime));
    }

    public Appointment transformAppmtCreateReq(AppointmentCreateRequest req, Instant endTime){

        String createdByDoctorId = req.getCreatedByDoctorId();
        String createdByPatientId = req.getCreatedByPatientId();

        return Appointment.builder()
                .id(UUID.randomUUID().toString())
                .doctor(Doctor.builder()
                        .id(req.getDoctorId())
                        .build())
                .patient(Patient.builder()
                        .id(req.getPatientId())
                        .build())
                .appointmentTime(req.getTime())
                .appointmentEndTime(endTime)
                .createdByDoctor(
                        StringUtils.hasText(createdByDoctorId) ?
                                Doctor.builder()
                                        .id(req.getCreatedByDoctorId())
                                        .build()
                                :
                                null
                )
                .createdByPatient(
                        StringUtils.hasText(createdByPatientId) ?
                                Patient.builder()
                                        .id(req.getCreatedByPatientId())
                                        .build()
                                :
                                null
                )
                .status(AppointmentStatus.ACTIVE)
                .createdAt(Instant.now())
                .build();
    }

    private Instant plusOneDay(Instant startTime){
        return startTime.plusSeconds(TOTAL_SECOND_IN_DAY);
    }

    //is appointment start and end time between 8 AM-4 PM
    private boolean isTimeOkay(Instant startTime, Instant endTime){

        ZonedDateTime zonedStart = startTime.atZone(ZoneId.of(CURRENT_TIME_ZONE));
        ZonedDateTime zonedEnd = endTime.atZone(ZoneId.of(CURRENT_TIME_ZONE));

        int startHour = zonedStart.getHour();
        int endHour = zonedEnd.getHour();
        int endMinute = zonedEnd.getMinute();
        int endSecond = zonedEnd.getSecond();

        return startHour >= 8 && startHour <= 16 &&
                endHour >= 8 && (endHour < 16 || (endHour == 16 && endMinute == 0 && endSecond == 0));
    }

}
