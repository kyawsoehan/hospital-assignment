package com.ksh.hospital.appointment.entity;

public enum AppointmentStatus {
    ACTIVE,
    CANCELLED;
}
