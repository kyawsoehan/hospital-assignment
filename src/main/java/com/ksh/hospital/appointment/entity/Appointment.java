package com.ksh.hospital.appointment.entity;

import com.ksh.hospital.doctor.entity.Doctor;
import com.ksh.hospital.patient.entity.Patient;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "appointments")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Appointment {

    @Id
    @Column(name = "id")
    private String id;

    @OneToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @OneToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name = "appointment_time")
    private Instant appointmentTime;

    @Column(name = "appointment_end_time")
    private Instant appointmentEndTime;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private AppointmentStatus status;

    @OneToOne
    @JoinColumn(name = "created_by_doctor_id")
    private Doctor createdByDoctor;

    @OneToOne
    @JoinColumn(name = "created_by_patient_id")
    private Patient createdByPatient;

}
