package com.ksh.hospital.appointment.repo;

import com.ksh.hospital.appointment.entity.Appointment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface AppointmentRepo extends CrudRepository<Appointment, String> {

    @Query(value = "select a from Appointment a JOIN FETCH a.doctor JOIN FETCH a.patient LEFT JOIN FETCH a.createdByDoctor LEFT JOIN FETCH a.createdByPatient " +
            "WHERE a.appointmentTime BETWEEN ?2 AND ?3 AND a.doctor.id = ?1 AND a.status = 'ACTIVE'")
    List<Appointment> findByDoctorIdWithTime(String doctorId, Instant startTime, Instant endTime);

    @Query(value = "select a from Appointment a JOIN FETCH a.doctor JOIN FETCH a.patient LEFT JOIN FETCH a.createdByDoctor LEFT JOIN FETCH a.createdByPatient " +
            "WHERE a.appointmentTime BETWEEN ?2 AND ?3 AND a.patient.id = ?1 AND a.status = 'ACTIVE'")
    List<Appointment> findByPatientIdWithTime(String patientId, Instant startTime, Instant endTime);

    @Query(value = "select a from Appointment a JOIN FETCH a.doctor JOIN FETCH a.patient LEFT JOIN FETCH a.createdByDoctor LEFT JOIN FETCH a.createdByPatient " +
            "WHERE (a.appointmentTime BETWEEN ?1 AND ?2 OR a.appointmentEndTime BETWEEN ?1 AND ?2) AND " +
            "(a.doctor.id = ?3 OR a.patient.id = ?4) AND a.status = 'ACTIVE'")
    List<Appointment> findBookedAppointments(Instant startTime, Instant endTime, String doctorId, String patientId);

    @Modifying
    @Query(value = "update Appointment a set a.status = 'CANCELLED' WHERE a.id = ?1 AND a.status = 'ACTIVE'")
    Integer cancelAppointment(String appointmentId);
}
