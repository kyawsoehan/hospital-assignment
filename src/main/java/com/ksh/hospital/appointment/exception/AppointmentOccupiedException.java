package com.ksh.hospital.appointment.exception;

import com.ksh.hospital.appointment.entity.Appointment;

import java.util.List;

public class AppointmentOccupiedException extends RuntimeException{

    private List<Appointment> occupiedList;

    public AppointmentOccupiedException(String message, List<Appointment> occupiedList){
        super(message);
        this.occupiedList = occupiedList;
    }

    public List<Appointment> getOccupiedList(){
        return this.occupiedList;
    }
}
