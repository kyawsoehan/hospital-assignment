package com.ksh.hospital.appointment.model;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentCreateRequest {

    @NotNull
    private Instant time;

    @NotNull
    @NotBlank
    private String doctorId;

    @NotNull
    @NotBlank
    private String patientId;

    private String createdByDoctorId;

    private String createdByPatientId;

}
