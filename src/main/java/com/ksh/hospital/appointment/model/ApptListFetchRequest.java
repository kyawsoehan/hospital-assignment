package com.ksh.hospital.appointment.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApptListFetchRequest {

    @NotNull
    @NotBlank
    private String id;

    @NotNull
    private Instant startTime;

}
