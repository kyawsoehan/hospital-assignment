package com.ksh.hospital.appointment.controller;

import com.ksh.hospital.appointment.entity.Appointment;
import com.ksh.hospital.appointment.exception.AppointmentOccupiedException;
import com.ksh.hospital.appointment.exception.InvalidAppointmentTimeException;
import com.ksh.hospital.appointment.model.AppointmentCreateRequest;
import com.ksh.hospital.appointment.model.ApptListFetchRequest;
import com.ksh.hospital.appointment.service.AppointmentService;
import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.patterns.HasMemberTypePatternForPerThisMatching;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("appointments")
@RequiredArgsConstructor
public class AppointmentController {

    private final AppointmentService service;

    @PostMapping("by-doctor")
    public List<Appointment> getAppointmentsByDoctor(@Valid @RequestBody ApptListFetchRequest req){
        return service.getAppointmentsByDoctorByDate(req.getId(), req.getStartTime());
    }

    @PostMapping("by-patient")
    public List<Appointment> getAppointmentsByPatient(@Valid @RequestBody ApptListFetchRequest req){
        return service.getAppointmentsByPatientByDate(req.getId(), req.getStartTime());
    }

    @PostMapping("create")
    public Appointment createAppointment(@Valid @RequestBody AppointmentCreateRequest req){
        return service.createAppointment(req);
    }

    @PutMapping("cancel/{appointmentId}")
    public String cancelAppointment(@PathVariable("appointmentId") String appointmentId){
        service.cancelAppointment(appointmentId);
        return "OK";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationErrors(MethodArgumentNotValidException ex){
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put("errorMessage", fieldName + " " + errorMessage);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidAppointmentTimeException.class)
    public Map<String, Object> handleInvalidAppointmentTimeException(InvalidAppointmentTimeException ex){
        Map<String, Object> errors = new HashMap<>();
        errors.put("errorMessage", ex.getMessage());
        errors.put("details", "Time must be between 8 AM - 4 PM");
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(AppointmentOccupiedException.class)
    public Map<String, Object> handleAppointmentOccupiedException(AppointmentOccupiedException ex){
        Map<String, Object> errors = new HashMap<>();
        errors.put("errorMessage", ex.getMessage());
        errors.put("details", ex.getOccupiedList());
        return errors;
    }

}
