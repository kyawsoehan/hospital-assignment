package com.ksh.hospital.patient.repo;

import com.ksh.hospital.patient.entity.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepo extends CrudRepository<Patient, String> {
}
