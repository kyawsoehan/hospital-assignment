package com.ksh.hospital.appointment.service;

import com.ksh.hospital.CommonConstants;
import com.ksh.hospital.appointment.entity.Appointment;
import com.ksh.hospital.appointment.entity.AppointmentStatus;
import com.ksh.hospital.appointment.exception.AppointmentOccupiedException;
import com.ksh.hospital.appointment.exception.InvalidAppointmentTimeException;
import com.ksh.hospital.appointment.model.AppointmentCreateRequest;
import com.ksh.hospital.appointment.repo.AppointmentRepo;
import com.ksh.hospital.doctor.entity.Doctor;
import com.ksh.hospital.patient.entity.Gender;
import com.ksh.hospital.patient.entity.Patient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.CollectionUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AppointmentServiceTest {

    @InjectMocks
    AppointmentService service;

    @Mock
    AppointmentRepo repo;

    /*
    *   @Param doctorId -> doctor id
    *   @Param startTime -> for particular date in UTC time
    */
    @Test
    @DisplayName("Test getAppointmentsByDoctorByDate")
    public void testGetAppointmentsByDoctorByDate(){

        Doctor d = getDoctor1();
        Patient p1 = getPatient1();
        Patient p2 = getPatient2();

        List<Appointment> appointments = List.of(
                this.getAppointment(d, p1, Instant.now().minusSeconds(7200)),
                this.getAppointment(d, p2, Instant.now().plusSeconds(7200))
        );

        String doctorId = "D1";
        Instant startTime = Instant.parse("2018-11-30T00:00:00.00Z");

        doReturn(appointments).when(repo).findByDoctorIdWithTime(eq(doctorId), any(Instant.class), any(Instant.class));

        ArgumentCaptor<String> doctorIdCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Instant> startTimeCaptor = ArgumentCaptor.forClass(Instant.class);
        ArgumentCaptor<Instant> endTimeCaptor = ArgumentCaptor.forClass(Instant.class);

        List<Appointment> returnedAppointments = service.getAppointmentsByDoctorByDate(doctorId, startTime);

        verify(repo, times(1))
                .findByDoctorIdWithTime(
                        doctorIdCaptor.capture(),
                        startTimeCaptor.capture(),
                        endTimeCaptor.capture()
                );

        //end time range should be 24 hrs from start time
        Instant expectedEndTime = startTime.plusSeconds(24 * 60 * 60);

        Assertions.assertEquals(doctorId, doctorIdCaptor.getValue());
        Assertions.assertEquals(startTime, startTimeCaptor.getValue());
        Assertions.assertEquals(expectedEndTime, endTimeCaptor.getValue());
        Assertions.assertEquals(appointments, returnedAppointments);

    }
    /*
    *   @Param patientId -> patient id
    *   @Param startTime -> for particular date in UTC time
    */
    @Test
    @DisplayName("Test getAppointmentsByPatientByDate")
    public void testGetAppointmentsByPatientByDate(){

        Doctor d = getDoctor1();
        Patient p1 = getPatient1();
        Patient p2 = getPatient2();

        List<Appointment> appointments = List.of(
                this.getAppointment(d, p1, Instant.now().minusSeconds(7200)),
                this.getAppointment(d, p2, Instant.now().plusSeconds(7200))
        );

        String patientId = "P1";
        Instant startTime = Instant.parse("2018-11-30T00:00:00.00Z");

        doReturn(appointments).when(repo).findByPatientIdWithTime(eq(patientId), any(Instant.class), any(Instant.class));

        ArgumentCaptor<String> patientIdCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Instant> startTimeCaptor = ArgumentCaptor.forClass(Instant.class);
        ArgumentCaptor<Instant> endTimeCaptor = ArgumentCaptor.forClass(Instant.class);

        List<Appointment> returnedAppointments = service.getAppointmentsByPatientByDate(patientId, startTime);

        verify(repo, times(1))
                .findByPatientIdWithTime(
                        patientIdCaptor.capture(),
                        startTimeCaptor.capture(),
                        endTimeCaptor.capture()
                );

        //end time range should be 24 hrs from start time
        Instant expectedEndTime = startTime.plusSeconds(24 * 60 * 60);

        Assertions.assertEquals(patientId, patientIdCaptor.getValue());
        Assertions.assertEquals(startTime, startTimeCaptor.getValue());
        Assertions.assertEquals(expectedEndTime, endTimeCaptor.getValue());
        Assertions.assertEquals(appointments, returnedAppointments);
    }


    /*
    *   @Param appointmentId -> ID of the appointment to be cancelled
    */
    @Test
    @DisplayName("Test cancelAppointment")
    public void testCancelAppointment(){

        String cancelledAppointment = "A1";

        doReturn(1).when(repo).cancelAppointment(eq(cancelledAppointment));

        ArgumentCaptor<String> appointmentIdCaptor = ArgumentCaptor.forClass(String.class);

        service.cancelAppointment(cancelledAppointment);

        verify(repo, times(1)).cancelAppointment(appointmentIdCaptor.capture());

        Assertions.assertEquals(cancelledAppointment, appointmentIdCaptor.getValue());

    }

    /*
    *   @Param AppointmentCreateRequest
    *       {
    *           "time" -> appointment start time,
    *           "doctorId" -> doctor id,
    *           "patientId" -> patient id,
    *           "createdByDoctorId" -> doctor id if this apptmnt is created by doctor,
    *           "createdByPatientId" -> patient id if this apptmnt is create by patient
    *       }
     */

    @Test
    @DisplayName("Test createAppointment")
    public void testCreateAppointment(){

        Doctor d = getDoctor1();
        Patient p1 = getPatient1();
        Patient p2 = getPatient2();

        List<Appointment> occupiedApptmntList = List.of(
                this.getAppointment(d, p1, Instant.now().minusSeconds(7200)),
                this.getAppointment(d, p2, Instant.now().plusSeconds(7200))
        );

        String doctorId = "D1";
        String patientId = "P1";
        Instant appointmentStartTime = Instant.parse("2018-11-30T00:00:00.00Z");
        Instant appointmentEndTime = appointmentStartTime.plusSeconds(CommonConstants.APPOINTMENT_DURATION_SECONDS);

        doReturn(occupiedApptmntList).when(repo)
                .findBookedAppointments(
                        eq(appointmentStartTime),
                        eq(appointmentEndTime),
                        eq(doctorId),
                        eq(patientId));

        //create new appointment, but there is appointments which conflict with current create request
        AppointmentCreateRequest createRequest = AppointmentCreateRequest.builder()
                .doctorId(doctorId)
                .patientId(patientId)
                .time(appointmentStartTime)
                .build();

        AppointmentOccupiedException exception = Assertions.assertThrows(
                AppointmentOccupiedException.class,
                () -> service.createAppointment(createRequest)
        );

        verify(repo, times(1))
                .findBookedAppointments(
                        eq(appointmentStartTime),
                        eq(appointmentEndTime),
                        eq(doctorId),
                        eq(patientId));

        //should not call save for appointment
        verify(repo, times(0))
                .save(any(Appointment.class));

        //error thrown contains conflicted appointment list
        Assertions.assertEquals(occupiedApptmntList, exception.getOccupiedList());




        //should create new request, if there is no conflict appointments
        Mockito.reset(repo);

        doctorId = "D1";
        patientId = "P1";
        appointmentStartTime = Instant.parse("2018-11-30T00:00:00.00Z");
        appointmentEndTime = appointmentStartTime.plusSeconds(CommonConstants.APPOINTMENT_DURATION_SECONDS);

        AppointmentCreateRequest createRequest2 = AppointmentCreateRequest.builder()
                .doctorId(doctorId)
                .patientId(patientId)
                .time(appointmentStartTime)
                .build();

        doReturn(List.of()).when(repo)
                .findBookedAppointments(
                        eq(appointmentStartTime),
                        eq(appointmentEndTime),
                        eq(doctorId),
                        eq(patientId));

        doReturn(null).when(repo)
                .save(any(Appointment.class));

        Assertions.assertDoesNotThrow(() -> service.createAppointment(createRequest2));

        verify(repo, times(1))
                .findBookedAppointments(
                        eq(appointmentStartTime),
                        eq(appointmentEndTime),
                        eq(doctorId),
                        eq(patientId));

        ArgumentCaptor<Appointment> appointmentArgumentCaptor = ArgumentCaptor.forClass(Appointment.class);

        verify(repo, times(1))
                .save(appointmentArgumentCaptor.capture());

        Appointment actualAppointment = appointmentArgumentCaptor.getValue();

        Assertions.assertNotNull(actualAppointment.getId());
        Assertions.assertEquals(doctorId, actualAppointment.getDoctor().getId());
        Assertions.assertEquals(patientId, actualAppointment.getPatient().getId());
        Assertions.assertEquals(appointmentStartTime, actualAppointment.getAppointmentTime());
        Assertions.assertEquals(appointmentEndTime, actualAppointment.getAppointmentEndTime());
        Assertions.assertNotNull(actualAppointment.getCreatedAt());
        Assertions.assertEquals(AppointmentStatus.ACTIVE, actualAppointment.getStatus());


        //time is not between 8 AM and 4 PM in SG Timezone
        Mockito.reset(repo);
        appointmentStartTime = Instant.parse("2018-11-30T17:00:00.00Z");
        AppointmentCreateRequest createRequest3 = AppointmentCreateRequest.builder()
                .doctorId(doctorId)
                .patientId(patientId)
                .time(appointmentStartTime)
                .build();

        Assertions.assertThrows(
                InvalidAppointmentTimeException.class,
                () -> service.createAppointment(createRequest3));

        //should not call db further
        verify(repo, times(0))
                .findBookedAppointments(
                        eq(appointmentStartTime),
                        eq(appointmentEndTime),
                        eq(doctorId),
                        eq(patientId));





        Mockito.reset(repo);
        appointmentStartTime = Instant.parse("2018-11-30T23:00:00.00Z");
        AppointmentCreateRequest createRequest4 = AppointmentCreateRequest.builder()
                .doctorId(doctorId)
                .patientId(patientId)
                .time(appointmentStartTime)
                .build();

        Assertions.assertThrows(
                InvalidAppointmentTimeException.class,
                () -> service.createAppointment(createRequest4));

        //should not call db further
        verify(repo, times(0))
                .findBookedAppointments(
                        eq(appointmentStartTime),
                        eq(appointmentEndTime),
                        eq(doctorId),
                        eq(patientId));


    }

    private Doctor getDoctor1(){
        return Doctor.builder()
                .id("D1")
                .name("D1 Name")
                .build();
    }

    private Patient getPatient1(){
        return Patient.builder()
                .id("P1")
                .name("P1 Name")
                .gender(Gender.M)
                .age(15)
                .build();
    }

    private Patient getPatient2(){
        return Patient.builder()
                .id("P2")
                .name("P2 Name")
                .gender(Gender.F)
                .age(20)
                .build();
    }

    private Appointment getAppointment(Doctor doc, Patient pat, Instant appmtTime){
        return Appointment.builder()
                .id(UUID.randomUUID().toString())
                .appointmentTime(appmtTime)
                .doctor(doc)
                .patient(pat)
                .createdAt(Instant.now())
                .build();
    }

}
