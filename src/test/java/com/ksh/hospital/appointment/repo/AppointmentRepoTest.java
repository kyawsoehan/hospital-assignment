package com.ksh.hospital.appointment.repo;

import com.ksh.hospital.CommonConstants;
import com.ksh.hospital.appointment.entity.Appointment;
import com.ksh.hospital.appointment.entity.AppointmentStatus;
import com.ksh.hospital.doctor.entity.Doctor;
import com.ksh.hospital.doctor.repo.DoctorRepo;
import com.ksh.hospital.patient.entity.Gender;
import com.ksh.hospital.patient.entity.Patient;
import com.ksh.hospital.patient.repo.PatientRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

@DataJpaTest
@TestPropertySource(properties = {
        "spring.datasource.url=jdbc:tc:postgresql:13.2-alpine:///hospital",
        "flyway.url=localhost:5432/hospital"
})
public class AppointmentRepoTest {

    @Autowired
    private AppointmentRepo appointmentRepo;

    @Autowired
    private PatientRepo patientRepo;

    @Autowired
    private DoctorRepo doctorRepo;

    @Autowired
    private EntityManager entityManager;

    @BeforeEach
    void resetDb(){
        this.appointmentRepo.deleteAll();
        this.patientRepo.deleteAll();
        this.doctorRepo.deleteAll();
    }

    /*
        @Param doctorId -> doctor id
        @Param startTime -> start date time according to different timezone in UTC format
        @Param endTime -> end date time according to different timezone in UTC format, normally it will be startTime + 24 hr
     */
    @Test
    @DisplayName("Test findByDoctorIdWithTime")
    public void testFindByDoctorIdWithTime(){

        Doctor doctor1 = getDoctor1();
        Patient patient1 = getPatient1();

        Doctor doctor2 = getDoctor2();
        Patient patient2 = getPatient2();

        this.doctorRepo.saveAll(List.of(doctor1, doctor2));
        this.patientRepo.saveAll(List.of(patient1, patient2));

        //insert three records with two different dates
        Instant apptmntTime1 = Instant.now();
        Appointment appointment1 = getAppointment(doctor1, patient1, apptmntTime1);

        Instant apptmntTime2 = Instant.now().plus(50, ChronoUnit.DAYS);
        Appointment appointment2 = getAppointment(doctor1, patient1, apptmntTime2);

        Instant apptmntTime3 = Instant.now().minus(30, ChronoUnit.DAYS);
        Appointment appointment3 = getAppointment(doctor2, patient2, apptmntTime3);

        //save to db first
        appointmentRepo.saveAll(List.of(appointment1, appointment2, appointment3));

        //test for appointment 1
        List<Appointment> appointments = appointmentRepo.findByDoctorIdWithTime(doctor1.getId(), apptmntTime1, apptmntTime1.plus(1, ChronoUnit.DAYS));

        Assertions.assertFalse(appointments.isEmpty());
        Assertions.assertEquals(1, appointments.size());

        Appointment appointment = appointments.get(0);
        Assertions.assertEquals(appointment1.getId(), appointment.getId());
        Assertions.assertEquals(appointment1.getAppointmentTime(), appointment.getAppointmentTime());
        Assertions.assertEquals(appointment1.getAppointmentEndTime(), appointment.getAppointmentEndTime());
        Assertions.assertEquals(AppointmentStatus.ACTIVE, appointment.getStatus());
        Assertions.assertEquals(doctor1.getId(), appointment.getDoctor().getId());



        //test for appointment 2
        appointments = appointmentRepo.findByDoctorIdWithTime(doctor1.getId(), apptmntTime2, apptmntTime2.plus(1, ChronoUnit.DAYS));

        Assertions.assertFalse(appointments.isEmpty());
        Assertions.assertEquals(1, appointments.size());

        appointment = appointments.get(0);
        Assertions.assertEquals(appointment2.getId(), appointment.getId());
        Assertions.assertEquals(appointment2.getAppointmentTime(), appointment.getAppointmentTime());
        Assertions.assertEquals(appointment2.getAppointmentEndTime(), appointment.getAppointmentEndTime());
        Assertions.assertEquals(AppointmentStatus.ACTIVE, appointment.getStatus());
        Assertions.assertEquals(doctor1.getId(), appointment.getDoctor().getId());




        //test for appointment 3
        appointments = appointmentRepo.findByDoctorIdWithTime(doctor2.getId(), apptmntTime3, apptmntTime3.plus(1, ChronoUnit.DAYS));

        Assertions.assertFalse(appointments.isEmpty());
        Assertions.assertEquals(1, appointments.size());

        appointment = appointments.get(0);
        Assertions.assertEquals(appointment3.getId(), appointment.getId());
        Assertions.assertEquals(appointment3.getAppointmentTime(), appointment.getAppointmentTime());
        Assertions.assertEquals(appointment3.getAppointmentEndTime(), appointment.getAppointmentEndTime());
        Assertions.assertEquals(AppointmentStatus.ACTIVE, appointment.getStatus());
        Assertions.assertEquals(doctor2.getId(), appointment.getDoctor().getId());
    }





    /*
        @Param patient id -> patient id
        @Param startTime -> start date time according to different timezone in UTC format
        @Param endTime -> end date time according to different timezone in UTC format, normally it will be startTime + 24 hr
     */
    @Test
    @DisplayName("Test findByPatientIdWithTime")
    public void testFindByPatientIdWithTime(){

        Doctor doctor1 = getDoctor1();
        Patient patient1 = getPatient1();

        Doctor doctor2 = getDoctor2();
        Patient patient2 = getPatient2();

        this.doctorRepo.saveAll(List.of(doctor1, doctor2));
        this.patientRepo.saveAll(List.of(patient1, patient2));

        //insert three records with two different dates
        Instant apptmntTime1 = Instant.now();
        Appointment appointment1 = getAppointment(doctor1, patient1, apptmntTime1);

        Instant apptmntTime2 = Instant.now().plus(50, ChronoUnit.DAYS);
        Appointment appointment2 = getAppointment(doctor1, patient1, apptmntTime2);

        Instant apptmntTime3 = Instant.now().minus(30, ChronoUnit.DAYS);
        Appointment appointment3 = getAppointment(doctor2, patient2, apptmntTime3);

        //save to db first
        appointmentRepo.saveAll(List.of(appointment1, appointment2, appointment3));

        //test for appointment 1
        List<Appointment> appointments = appointmentRepo.findByPatientIdWithTime(patient1.getId(), apptmntTime1, apptmntTime1.plus(1, ChronoUnit.DAYS));

        Assertions.assertFalse(appointments.isEmpty());
        Assertions.assertEquals(1, appointments.size());

        Appointment appointment = appointments.get(0);
        Assertions.assertEquals(appointment1.getId(), appointment.getId());
        Assertions.assertEquals(appointment1.getAppointmentTime(), appointment.getAppointmentTime());
        Assertions.assertEquals(appointment1.getAppointmentEndTime(), appointment.getAppointmentEndTime());
        Assertions.assertEquals(AppointmentStatus.ACTIVE, appointment.getStatus());
        Assertions.assertEquals(patient1.getId(), appointment.getPatient().getId());



        //test for appointment 2
        appointments = appointmentRepo.findByPatientIdWithTime(patient1.getId(), apptmntTime2, apptmntTime2.plus(1, ChronoUnit.DAYS));

        Assertions.assertFalse(appointments.isEmpty());
        Assertions.assertEquals(1, appointments.size());

        appointment = appointments.get(0);
        Assertions.assertEquals(appointment2.getId(), appointment.getId());
        Assertions.assertEquals(appointment2.getAppointmentTime(), appointment.getAppointmentTime());
        Assertions.assertEquals(appointment2.getAppointmentEndTime(), appointment.getAppointmentEndTime());
        Assertions.assertEquals(AppointmentStatus.ACTIVE, appointment.getStatus());
        Assertions.assertEquals(patient1.getId(), appointment.getPatient().getId());




        //test for appointment 3
        appointments = appointmentRepo.findByPatientIdWithTime(patient2.getId(), apptmntTime3, apptmntTime3.plus(1, ChronoUnit.DAYS));

        Assertions.assertFalse(appointments.isEmpty());
        Assertions.assertEquals(1, appointments.size());

        appointment = appointments.get(0);
        Assertions.assertEquals(appointment3.getId(), appointment.getId());
        Assertions.assertEquals(appointment3.getAppointmentTime(), appointment.getAppointmentTime());
        Assertions.assertEquals(appointment3.getAppointmentEndTime(), appointment.getAppointmentEndTime());
        Assertions.assertEquals(AppointmentStatus.ACTIVE, appointment.getStatus());
        Assertions.assertEquals(patient2.getId(), appointment.getPatient().getId());
    }


    /*
        @Param startTime -> start date time according to different timezone in UTC format
        @Param endTime -> end date time according to different timezone in UTC format, normally it will be startTime + 24 hr
        @Param doctorId -> id of doctor
        @Param patientId -> id of patient
     */
    @Test
    @DisplayName("Test findBookedAppointments")
    void testFindBookedAppointments(){

        Doctor doctor1 = getDoctor1();
        Patient patient1 = getPatient1();

        Doctor doctor2 = getDoctor2();
        Patient patient2 = getPatient2();

        this.doctorRepo.saveAll(List.of(doctor1, doctor2));
        this.patientRepo.saveAll(List.of(patient1, patient2));

        //insert three records with two different dates
        Instant apptmntTime1 = Instant.now();
        Appointment appointment1 = getAppointment(doctor1, patient1, apptmntTime1);

        Instant apptmntTime2 = Instant.now().plus(50, ChronoUnit.DAYS);
        Appointment appointment2 = getAppointment(doctor1, patient1, apptmntTime2);

        Instant apptmntTime3 = Instant.now().minus(30, ChronoUnit.DAYS);
        Appointment appointment3 = getAppointment(doctor2, patient2, apptmntTime3);

        //save to db first
        appointmentRepo.saveAll(List.of(appointment1, appointment2, appointment3));

        //query with random doctor and patient id
        List<Appointment> appointments = appointmentRepo.findBookedAppointments(
                apptmntTime1,
                apptmntTime1.plusSeconds(60 * 60),
                "random doctor id",
                "random patient id"
        );

        Assertions.assertTrue(appointments.isEmpty());

        //query with correct doctor and start time is between appointment1
        appointments = appointmentRepo.findBookedAppointments(
                apptmntTime1.plusSeconds(30 * 60),
                apptmntTime1.plusSeconds(60 * 60),
                doctor1.getId(),
                "random patient id"
        );

        Assertions.assertFalse(appointments.isEmpty());
        Appointment appointment = appointments.get(0);

        Assertions.assertEquals(appointment1.getId(), appointment.getId());
        Assertions.assertEquals(appointment1.getDoctor().getId(), appointment.getDoctor().getId());


        //query with correct patient and start time is between appointment1
        appointments = appointmentRepo.findBookedAppointments(
                apptmntTime1.plusSeconds(30 * 60),
                apptmntTime1.plusSeconds(60 * 60),
                "random doctor id",
                appointment1.getPatient().getId()
        );

        Assertions.assertFalse(appointments.isEmpty());
        appointment = appointments.get(0);

        Assertions.assertEquals(appointment1.getId(), appointment.getId());
        Assertions.assertEquals(appointment1.getPatient().getId(), appointment.getPatient().getId());

        //query with correct doctor and patient
        appointments = appointmentRepo.findBookedAppointments(
                apptmntTime3.plusSeconds(30 * 60),
                apptmntTime3.plusSeconds(60 * 60),
                appointment3.getDoctor().getId(),
                appointment3.getPatient().getId()
        );

        Assertions.assertFalse(appointments.isEmpty());
        appointment = appointments.get(0);

        Assertions.assertEquals(appointment3.getId(), appointment.getId());
        Assertions.assertEquals(appointment3.getPatient().getId(), appointment.getPatient().getId());

    }



    @Test
    @DisplayName("Test cancelAppointment")
    void testCancelAppointment(){

        Doctor doctor1 = getDoctor1();
        Patient patient1 = getPatient1();

        this.doctorRepo.saveAll(List.of(doctor1));
        this.patientRepo.saveAll(List.of(patient1));

        //insert three records with two different dates
        Instant apptmntTime1 = Instant.now();
        Appointment appointment1 = getAppointment(doctor1, patient1, apptmntTime1);

        Instant apptmntTime2 = Instant.now().plus(50, ChronoUnit.DAYS);
        Appointment appointment2 = getAppointment(doctor1, patient1, apptmntTime2);

        this.appointmentRepo.saveAll(List.of(appointment1, appointment2));

        //cancel appointment 1
        this.appointmentRepo.cancelAppointment(appointment1.getId());
        this.entityManager.clear();

        Appointment resultAppointment1 = this.appointmentRepo.findById(appointment1.getId()).get();
        Appointment resultAppointment2 = this.appointmentRepo.findById(appointment2.getId()).get();

        Assertions.assertEquals(AppointmentStatus.CANCELLED, resultAppointment1.getStatus());
        Assertions.assertEquals(AppointmentStatus.ACTIVE, resultAppointment2.getStatus());


        //cancel appointment 2
        this.appointmentRepo.cancelAppointment(appointment2.getId());
        this.entityManager.clear();

        resultAppointment1 = this.appointmentRepo.findById(appointment1.getId()).get();
        resultAppointment2 = this.appointmentRepo.findById(appointment2.getId()).get();

        Assertions.assertEquals(AppointmentStatus.CANCELLED, resultAppointment1.getStatus());
        Assertions.assertEquals(AppointmentStatus.CANCELLED, resultAppointment2.getStatus());

    }

    private Patient getPatient1(){
        return Patient.builder()
                .id("P1")
                .age(12)
                .name("P1 Name")
                .gender(Gender.M)
                .build();
    }

    private Patient getPatient2(){
        return Patient.builder()
                .id("P2")
                .age(15)
                .name("P2 Name")
                .gender(Gender.F)
                .build();
    }

    private Doctor getDoctor1(){
        return Doctor.builder()
                .id("D1")
                .name("D1 Name")
                .build();
    }

    private Doctor getDoctor2(){
        return Doctor.builder()
                .id("D2")
                .name("D2 Name")
                .build();
    }

    private Appointment getAppointment(Doctor doctor, Patient patient, Instant startTime){
        return Appointment.builder()
                .id(UUID.randomUUID().toString())
                .doctor(doctor)
                .patient(patient)
                .appointmentTime(startTime)
                .appointmentEndTime(startTime.plusSeconds(CommonConstants.APPOINTMENT_DURATION_SECONDS))
                .status(AppointmentStatus.ACTIVE)
                .createdAt(Instant.now())
                .build();
    }

}
